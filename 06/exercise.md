# Include a pipeline

Extend the existing .gitlab-ci.yml configuration by adding Static Application Security Testing provided by Gitlab
https://docs.gitlab.com/ee/user/application_security/sast/