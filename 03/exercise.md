# Create a simple cache

Extend the existing .gitlab-ci.yml configuration by passing the output of the build job as artifact to the docker job.

- The output of the application is stored under `app/.output/public`