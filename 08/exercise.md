# Deploy the ci-metrics scrapper

1) Adjust the configuration to your project
2) Execute `kubectl create configmap ci-metrics-config --from-file=ci-metrics.config`
3) Execute `kubectl create secret generic gitlab-token --from-literal=token=<yourtoken>`
4) Deploy application
5) Portforward to access port 8080