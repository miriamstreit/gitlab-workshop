# feature-branch deployment

- Install argocd https://argo-cd.readthedocs.io/en/stable/getting_started
    ```
    kubectl create namespace argocd
    kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
    ```

    You can access the UI with port-forwading
    ```
     kubectl port-forward svc/argocd-server -n argocd 8080:443
    ```

    The secrect is stored as `argocd-initial-admin-secret`

- Create a new gitlab project to host the argocd application definition and commit the argocd-apps directory

- Create a public docker account to be able to use the docker registry

- Adjust the existing .gitlab-ci.yml configuration to your environment:

- Set the following variables within your Gitlab Pipeline

| Key                  | Value                 | Description                |
|----------------------|-----------------------|----------------------------|
| `GIT_USER`           | dummy                 | but must be defined        |
| `GIT_TOKEN`          | Token                 | Need API write access      |
| `ARGOCD_GIT_URL`     | URL                   | without `https://`         |
| `ARGOCD_DEPLOY_REPO` | Name of the repo      |                            |
| `REGISTRY_PROJECT`   | Docker                |                            |
| `REGISTRY_USER`      | Docker                |                            |
| `REGISTRY_TOKEN`     | Docker                |                            |


 - Create the namespace `workshop`on your cluster

 - Test the workflow

 - Bonus: Add and ingress based and the logic to adjust it to the lower_case_branch name